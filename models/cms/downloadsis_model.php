<?php
/**
 * Modelo del sistema de descargas
 */
class Downloadsis_model extends MY_Model {
	
	function __construct() {
		parent::__construct();
	}
	
	/**
	 * SISTEMA DE DESCARGAS - Obtener categorías
	 * @param $current Enviar para obtener datos de una categoría específica
	 */
	 public function downloadsis_categories_lists($current = NULL){
	 	$query = $this->db->from('PLUGIN_DOWNLOADSIS_CATEGORIES');
		$query = (!empty($current))?$query->where('ID = '.$current):$query;
		$query = $query->get();
		
		return (!empty($current))?$query->row():$query->result();
	 }
	/**
	 * SISTEMA DE DESCARGAS - Obtener tipos
	 * @param $current Enviar para obtener datos de una categoría específica
	 */
	 public function downloadsis_types_lists($current = NULL){
	 	$query = $this->db->from('PLUGIN_DOWNLOADSIS_TYPES');
		$query = (!empty($current))?$query->where('ID = '.$current):$query;
		$query = $query->get();
		
		return (!empty($current))?$query->row():$query->result();
	 }
	 /**
	  * Array de categorías
	  */
	  public function categories_array(){
	  	$query = $this->downloadsis_categories_lists();
		$categories = array();
		foreach($query as $category){
			$categories[$category->ID] = $category->CATEGORY_NAME;
		}
		return $categories;
	  }
	 /**
	  * Array de tipos
	  */
	  public function types_array(){
	  	$query = $this->downloadsis_types_lists();
		$categories = array();
		foreach($query as $category){
			$categories[$category->ID] = $category->TYPE_NAME;
		}
		return $categories;
	  }
}
