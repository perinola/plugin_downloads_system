<?php
/**
 * @author 	Guido A. Orellana
 * @name	Plugin Archivos (Sistema de Descargas)
 * @since	octubre 2013
 * 
 */
class Plugin_descargas_archivos extends PL_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->model('cms/downloadsis_model', 'downloadsis');
		
		//Load the plugin data
		$this->plugin_action_table			= 'PLUGIN_DOWNLOADSIS_FILES';
		$this->plugin_button_create			= "Crear Nuevo Archivo";
		$this->plugin_button_cancel			= "Cancelar";
		$this->plugin_button_update			= "Guardar Cambios";
		$this->plugin_button_delete			= "Eliminar";
		$this->plugin_page_title			= "Archivos";
		$this->plugin_page_create			= "Crear Nuevo Archivo";
		$this->plugin_page_read				= "Mostrar Archivo";
		$this->plugin_page_update			= "Editar Archivo";
		$this->plugin_page_delete			= "Eliminar";
		$this->plugin_display_array[0]		= "ID";
		$this->plugin_display_array[1]		= "Nombre";
		$this->plugin_display_array[2]		= "Archivo";
		$this->plugin_display_array[3]		= "Tipo";
		$this->plugin_display_array[4]		= "Categor&iacute;a";
		$this->plugin_display_array[5]		= "Descripci&oacute;n";
		
		$this->plugins_model->initialise($this->plugin_action_table);
		
		$this->categories					= $this->downloadsis->categories_array();
		$this->types						= $this->downloadsis->types_array();
		
		//Extras to send
		$this->plugin_image_route			= "/user_files/uploads/";
		$this->file_input					= "FILE_UPLOAD";
		
		$this->display_pagination			= TRUE; //Mostrar paginaci�n en listado
		$this->pagination_per_page			= 10; //Numero de registros por p�gina
		$this->pagination_total_rows		= $this->plugins_model->total_rows(); //N�mero total de items a desplegar
		
		$this->display_filter				= FALSE; //Mostrar filtro de b�squeda 'SEARCH' o seg�n listado 'LIST' o no mostrar FALSE
	}
	
	/**
	 * Funci�n para desplegar listado completo de datos guardados, enviar los t�tulos en array con clave header y el cuerpo en un array con clave body.
	 * Para editar fila es a la funci�n 'update_table_row'
	 * 
	 * @param	$result_array 		array 		Array con la listado devuelto por query de la DB
	 * @return	$data_array 		array 		Arreglo con la informaci�n del [header] y [body]
	 * 											Env�a el key [filteroptions] con las opciones de un filtro en listado y [currentFilter] con el resultado actual.
	 */
	public function _html_plugin_display($result_array){
		
		//Header data
		$data_array['header'][1]			= $this->plugin_display_array[3];
		$data_array['header'][2]			= $this->plugin_display_array[1];
		$data_array['header'][3]			= $this->plugin_display_array[4];
		$data_array['header'][4]			= $this->plugin_display_array[2];
		
		//Body data
		$data_array['body'] = '';
		foreach($result_array['body'] as $field):
		//Obtener datos de categor�as y tipos
		$category							= $this->downloadsis->downloadsis_categories_lists($field->FILE_CATEGORY);
		$type								= $this->downloadsis->downloadsis_types_lists($field->FILE_TYPE);
		
		$data_array['body']					.= '<tr>';
		$data_array['body']					.= '<td class="span4"><img src="'.base_url($this->plugin_image_route.'images/'.$type->TYPE_IMAGE).'" /></td>';
		$data_array['body']					.= '<td><a href="'.base_url('cms/'.strtolower($this->current_plugin).'/update_table_row/'.$field->ID).'">'.$field->FILE_NAME.'</a></td>';
		$data_array['body']					.= '<td class="span4">'.$category->CATEGORY_NAME.'</td>';
		$data_array['body']					.= '<td class="span4"><a class="btn" href="'.base_url($this->plugin_image_route.$field->FILE_UPLOAD).'" target="_blank" ><i class="icon-download-alt"></i> Descargar</a></td>';
		$data_array['body']					.= '</tr>';
		endforeach;
		
		return $data_array;
	}
	
	/*
	 * Funci�n para crear nuevo contenido, desde aqu� se especifican los campos a enviar en el formulario.
	 * El formulario se env�a mediante objectos preestablecidos de codeigniter. 
	 * El formulario se env�a con un array con la clave form_html.
	 * Se puede encontrar una gu�a en: http://ellislab.com/codeigniter/user-guide/helpers/form_helper.html
	 */
	public function _html_plugin_create(){
        
		//Formulario
		$data_array['form_html']			 = "<div class='control-group'>".form_label($this->plugin_display_array[1],'',array('class' => 'control-label'))."<div class='controls'>".form_input(array('name' => 'FILE_NAME', 'class' => 'span6'))."</div></div>";
		$data_array['form_html']			.= "<div class='control-group'>".form_label($this->plugin_display_array[2],'',array('class' => 'control-label'))."<div class='controls'>".form_upload(array('name' => 'FILE_UPLOAD', 'class' => 'span6'))."</div></div>";
		$data_array['form_html']			.= "<div class='control-group'>".form_label($this->plugin_display_array[4],'',array('class' => 'control-label'))."<div class='controls'>".form_dropdown('FILE_CATEGORY', $this->categories)."</div></div>";
		$data_array['form_html']			.= "<div class='control-group'>".form_label($this->plugin_display_array[3],'',array('class' => 'control-label'))."<div class='controls'>".form_dropdown('FILE_TYPE', $this->types)."</div></div>";
		$data_array['form_html']			.= "<div class='control-group'>".form_label($this->plugin_display_array[5],'',array('class' => 'control-label'))."<div class='controls'>".form_textarea(array('name' => 'FILE_DETAIL', 'class' => 'span6 textarea'))."</div></div>";
		
		return $data_array;
    }
	public function _html_plugin_update($result_data){
		
		//Formulario
		$data_array['form_html']			 = "<div class='control-group'>".form_label($this->plugin_display_array[1],'',array('class' => 'control-label'))."<div class='controls'>".form_input(array('name' => 'FILE_NAME', 'value' => $result_data->FILE_NAME, 'class' => 'span6'))."</div></div>";
		$data_array['form_html']			.= "<div class='control-group'>".form_label($this->plugin_display_array[2],'',array('class' => 'control-label'))."<div class='controls'>".form_upload(array('name' => 'FILE_UPLOAD', 'class' => 'span6'))."</div></div>";
		$data_array['form_html']			.= "<div class='control-group'>".form_label($this->plugin_display_array[4],'',array('class' => 'control-label'))."<div class='controls'>".form_dropdown('FILE_CATEGORY', $this->categories, $result_data->FILE_CATEGORY)."</div></div>";
		$data_array['form_html']			.= "<div class='control-group'>".form_label($this->plugin_display_array[3],'',array('class' => 'control-label'))."<div class='controls'>".form_dropdown('FILE_TYPE', $this->types, $result_data->FILE_TYPE)."</div></div>";
		$data_array['form_html']			.= "<div class='control-group'>".form_label($this->plugin_display_array[5],'',array('class' => 'control-label'))."<div class='controls'>".form_textarea(array('name' => 'FILE_DETAIL', 'value' => $result_data->FILE_DETAIL, 'class' => 'span6 textarea'))."</div></div>";
		
		return $data_array;
	}
	
	
	/**
	 * Funciones para editar Querys o Datos a enviar desde cada plugin
	 */
	//Funci�n para desplegar listado, desde aqu� se puede modificar el query
	public function _plugin_display($filter){
		$offset = (isset($filter[2]))?$filter[2]:0;
		$result_array['body'] = $this->plugins_model->list_rows('', '', $this->pagination_per_page, $offset, 'FILE_DATE DESC');
		
		
		return $this->_html_plugin_display($result_array);
	}
	//Funciones de los posts a enviar
	public function post_new_val(){
		$submit_posts 				= $this->input->post();
		$addimg = $this->upload_image();
		$submit_posts['FILE_DATE']		= date('Y-m-d');
		$submit_posts['FILE_UPLOAD']	= ($addimg)?$addimg['file_name']:NULL;
		
		return $this->_set_new_val($submit_posts);
	}
	public function post_update_val($data_id){
		$submit_posts 				= $this->input->post();
		$submit_posts['ID']			= $data_id;
		$addimg = $this->upload_image();
		if($addimg)
		$submit_posts['FILE_UPLOAD']	= $addimg['file_name'];
				
		return $this->_set_update_val($submit_posts);
	}
	
	/**
	 * Funciones espec�ficas del plugin
	 */
	 private function upload_image(){
	 	
			//Si se carga una imagen
			if(!empty($_FILES[$this->file_input]["name"])):
				$upload_config['upload_path'] 		= '.'.$this->plugin_image_route;
				$upload_config['allowed_types'] 	= '*';
				$upload_config['overwrite']			= TRUE;
				$this->upload->initialize($upload_config);
				
				if (!$this->upload->do_upload($this->file_input)):
					return false;
					$this->fw_alerts->add_new_alert(4002, 'ERROR');
				else:
					return $uploaded_data = $this->upload->data();
					$this->fw_alerts->add_new_alert(4001, 'SUCCESS');
				endif;
			endif;
	 }
}